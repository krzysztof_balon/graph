package graph.matrix;

import graph.Edge;
import graph.Graph;

import java.util.Arrays;
import java.util.Optional;

public class MGraph implements Graph {
    private final Integer[][] matrix;

    public MGraph(int size) {
        this.matrix = new Integer[size][size];
    }

    @Override
    public int[] getVertices() {
        int[] vertices = new int[matrix.length];
        int verticesCount = 0;
        for (int vertexIndex = 0; vertexIndex < matrix.length; vertexIndex++) {
            Integer[] weights = matrix[vertexIndex];
            boolean hasEdge = Arrays.asList(weights).parallelStream()
                    .anyMatch(weight -> weight != null);
            if (hasEdge) {
                vertices[verticesCount++] = vertexIndex;
            }
        }
        return Arrays.copyOf(vertices, verticesCount);
    }

    @Override
    public Edge[] getEdges() {
        Edge[] edges = new Edge[matrix.length * matrix.length];
        int edgesCount = 0;
        for (int src = 0; src < matrix.length; src++) {
            for (int target = 0; target < matrix.length; target++) {
                Integer weight = matrix[src][target];
                if (weight != null) {
                    edges[edgesCount++] = new Edge(src, target, weight);
                }
            }
        }
        return Arrays.copyOf(edges, edgesCount);
    }

    @Override
    public Optional<Edge> getEdge(int srcVertex, int targetVertex) {
        Integer weight = matrix[srcVertex][targetVertex];
        return weight == null
                ? Optional.<Edge>empty()
                : Optional.of(new Edge(srcVertex, targetVertex, weight));
    }

    @Override
    public void removeEdge(int srcVertex, int targetVertex) {
        matrix[srcVertex][targetVertex] = null;
    }

    @Override
    public void editEdge(Edge edge) {
        matrix[edge.getSrcVertex()][edge.getTargetVertex()] = edge.getWeight();
    }

    @Override
    public Edge[] getEdgesFrom(int srcVertex) {
        Edge[] edges = new Edge[matrix.length];
        int edgesCount = 0;
        for (int target = 0; target < matrix.length; target++) {
            Integer weight = matrix[srcVertex][target];
            if (weight != null) {
                edges[edgesCount++] = new Edge(srcVertex, target, weight);
            }
        }
        return Arrays.copyOf(edges, edgesCount);
    }

    @Override
    public void addEdge(Edge edge) {
        matrix[edge.getSrcVertex()][edge.getTargetVertex()] = edge.getWeight();
    }
}
