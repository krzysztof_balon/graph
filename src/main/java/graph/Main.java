package graph;

import algo.FordBellmanAlgorithm;
import algo.FordFulkersonAlgorithm;
import org.apache.commons.lang3.ArrayUtils;
import org.javatuples.Pair;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Main {
    public static void main(String... args) throws IOException, URISyntaxException {
        Graph graph = loadGraph(GraphLoader.mGraphLoader, "duzy_graf.txt");
        calculateMinPaths(graph, 109, 609);
//        calculateMaxFlow(graph);
//        System.out.println(buildPath(new PathFinder(graph).getPath(109, 609)));
    }

    public static void calculateMinPaths(Graph graph, int srcVertex, int targetVertex) {
        long start = System.currentTimeMillis();
        Pair<Integer[], Integer[]> result = new FordBellmanAlgorithm().execute(graph, srcVertex);
        System.out.println("Execution time: " + (System.currentTimeMillis() - start) + "ms.");

        Integer[] weights = result.getValue0();
        Integer[] predecessors = result.getValue1();
        for (int i = 0; i < weights.length; i++) {
            if (weights[i] != null) {
                System.out.println("Min path distance " + srcVertex + " -> " + i + " = " + weights[i]);
            }
        }
        Integer[] targetVertexPredecessors = new Integer[predecessors.length];
        int targetVertexPredecessorsCount = 0;
        Integer predecessor = targetVertex;
        while (predecessor != null) {
            targetVertexPredecessors[targetVertexPredecessorsCount++] = predecessor;
            predecessor = predecessors[predecessor];
        }
        Integer[] path = Arrays.copyOf(targetVertexPredecessors, targetVertexPredecessorsCount);
        ArrayUtils.reverse(path);
        StringBuilder pathBuilder = new StringBuilder("Path from ").append(srcVertex)
                .append(" to ").append(targetVertex).append(": ");
        pathBuilder.append(Arrays.stream(path).map(Object::toString).collect(Collectors.joining(" -> ")));
        System.out.println(pathBuilder.toString());
        System.out.println("Distance " + srcVertex + " -> " + targetVertex + " : " + weights[targetVertex]);
    }

    public static void calculateMaxFlow(Graph graph) {
        long start = System.currentTimeMillis();
        System.out.println("Max flow from 109 to 609: " + new FordFulkersonAlgorithm(graph).getMaxFlow(109, 609));
        System.out.println("Execution time: " + (System.currentTimeMillis() - start) / 1000 + "s.");
    }

    public static Graph loadGraph(GraphLoader graphLoader, String graphPath) throws URISyntaxException, IOException {
        long startTime = System.currentTimeMillis();
        Graph graph = graphLoader
                .loadGraph(Paths.get(ClassLoader.getSystemResource(graphPath).toURI()));
        System.out.println("Execution in: " + (System.currentTimeMillis() - startTime) + "ms. Vertices: " +
                graph.getVertices().length + " Edges: " + graph.getEdges().length);
        return graph;
    }

    public static String buildPath(Edge[] path) {
        if (path == null) {
            return "No path found.";
        } else if (path.length == 0) {
            return "No edges in path.";
        }
        StringBuilder pathBuilder = new StringBuilder().append(path[0].getSrcVertex());
        for (Edge edge : path) {
            pathBuilder.append(" -> ").append(edge.getTargetVertex());
        }
        return pathBuilder.toString();
    }
}
