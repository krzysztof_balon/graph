package graph;

import java.util.Optional;

public interface Graph {
    int[] getVertices();
    Edge[] getEdges();
    Optional<Edge> getEdge(int srcVertex, int targetVertex);
    void removeEdge(int srcVertex, int targetVertex);
    void editEdge(Edge edge);
    Edge[] getEdgesFrom(int srcVertex);
    void addEdge(Edge edge);
}
