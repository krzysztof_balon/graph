package graph.neighbor;

import graph.Edge;
import graph.Graph;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class NGraph implements Graph {
    private final Vertex[] vertices;

    public NGraph(int size) {
        this.vertices = new Vertex[size];
    }

    @Override
    public int[] getVertices() {
        int[] realVertices = new int[vertices.length];
        int verticesCount = 0;
        for (int i = 0; i < vertices.length; i++) {
            if (vertices[i] != null) {
                realVertices[verticesCount++] = i;
            }
        }
        return Arrays.copyOf(realVertices, verticesCount);
    }

    @Override
    public Edge[] getEdges() {
        return Arrays.asList(vertices).parallelStream()
                .filter(Objects::nonNull)
                .flatMap(vertex -> Arrays.asList(vertex.getOutEdges()).parallelStream())
                .toArray(Edge[]::new);
    }

    @Override
    public Optional<Edge> getEdge(int srcVertex, int targetVertex) {
        return Arrays.asList(vertices[srcVertex].getOutEdges()).parallelStream()
                .filter(edge -> edge.getTargetVertex() == targetVertex)
                .findFirst();
    }

    @Override
    public void removeEdge(int srcVertexIndex, int targetVertexIndex) {
        Vertex srcVertex = vertices[srcVertexIndex];
        Edge edgeToRemove = Arrays.asList(srcVertex.getOutEdges()).parallelStream()
                .filter(edge -> edge.getTargetVertex() == targetVertexIndex)
                .findFirst().get();
        srcVertex.removeOutEdge(edgeToRemove);
    }

    @Override
    public void editEdge(Edge edge) {
        Vertex srcVertex = vertices[edge.getSrcVertex()];
        Edge edgeToReplace = Arrays.asList(srcVertex.getOutEdges()).parallelStream()
                .filter(srcVertexEdge -> srcVertexEdge.getTargetVertex() == edge.getTargetVertex())
                .findFirst().get();
        srcVertex.replaceOutEdge(edgeToReplace, edge);
    }

    @Override
    public Edge[] getEdgesFrom(int srcVertex) {
        return vertices[srcVertex].getOutEdges();
    }

    @Override
    public void addEdge(Edge edge) {
        Vertex srcVertex = getOrCreateVertex(edge.getSrcVertex());
        initVertex(edge.getTargetVertex());
        srcVertex.addOutEdge(edge);
    }

    private Vertex getOrCreateVertex(int vertexIndex) {
        initVertex(vertexIndex);
        return vertices[vertexIndex];
    }

    private void initVertex(int vertexIndex) {
        if (vertices[vertexIndex] == null) {
            vertices[vertexIndex] = new Vertex();
        }
    }
}
