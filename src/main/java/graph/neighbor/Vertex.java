package graph.neighbor;

import graph.Edge;

import java.util.Arrays;
import java.util.Objects;

public class Vertex {
    private static final int INITIAL_SIZE = 16;
    private Edge[] outEdges = new Edge[INITIAL_SIZE];
    private int outEdgesCount = 0;

    public Edge[] getOutEdges() {
        return Arrays.asList(outEdges).parallelStream().filter(Objects::nonNull).toArray(Edge[]::new);
    }

    public void addOutEdge(Edge edge) {
        expandOutEdges();
        outEdges[outEdgesCount++] = edge;
    }

    public void replaceOutEdge(Edge current, Edge newEdge) {
        int edgePosition = findOutEdge(current);
        outEdges[edgePosition] = newEdge;
    }

    public void removeOutEdge(Edge edge) {
        int edgePosition = findOutEdge(edge);
        outEdges[edgePosition] = null;
    }

    private void expandOutEdges() {
        if (outEdgesCount == outEdges.length) {
            Edge[] newEdges = new Edge[outEdges.length * 2];
            int i = 0;
            for (Edge edge : outEdges) {
                if (edge != null) {
                    newEdges[i++] = edge;
                }
            }
            outEdges = newEdges;
        }
    }

    private int findOutEdge(Edge edge) {
        for (int i = 0; i < outEdgesCount; i++) {
            if (edge == outEdges[i]) {
                return i;
            }
        }
        throw new IllegalArgumentException("Shouldn't happen");
    }
}
