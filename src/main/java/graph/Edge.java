package graph;

import lombok.Value;

@Value
public class Edge {
    private final int srcVertex;
    private final int targetVertex;
    private final int weight;
}
