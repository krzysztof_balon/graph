package graph;

import graph.matrix.MGraph;
import graph.neighbor.NGraph;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Stream;

public abstract class GraphLoader {
    public Graph loadGraph(Path graphFile) throws IOException {
        int graphSize = findGraphSize(graphFile);
        System.out.println("Graph size: " + graphSize);
        Graph graph = createGraph(graphSize);
        fillGraph(graphFile, graph);
        return graph;
    }

    private int findGraphSize(Path graphFile) throws IOException {
        try (Stream<String> lines = Files.lines(graphFile)) {
            return lines.map(line -> line.split("; "))
                    .map(edge -> new String[]{edge[0], edge[1]})
                    .flatMap(vertices -> Arrays.asList(vertices).stream())
                    .mapToInt(Integer::parseInt)
                    .max().getAsInt() + 1;
        }
    }

    private void fillGraph(Path graphFile, Graph graph) throws IOException {
        try (Stream<String> lines = Files.lines(graphFile)) {
            lines.map(line -> line.split("; ")).forEach(textEdge -> {
                int[] edge = Arrays.asList(textEdge).stream().mapToInt(Integer::parseInt).toArray();
                graph.addEdge(new Edge(edge[0], edge[1], edge[2]));
            });
        }
    }

    protected abstract Graph createGraph(int size);

    public static final GraphLoader mGraphLoader = new GraphLoader() {
        @Override
        protected Graph createGraph(int size) {
            return new MGraph(size);
        }
    };

    public static final GraphLoader nGraphLoader = new GraphLoader() {
        @Override
        protected Graph createGraph(int size) {
            return new NGraph(size);
        }
    };
}
