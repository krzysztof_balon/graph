package algo;

import graph.Edge;
import graph.Graph;
import org.javatuples.Pair;

import java.util.Arrays;

public class FordBellmanAlgorithm {
    public Pair<Integer[], Integer[]> execute(Graph graph, int srcVertex) {
        int[] vertices = graph.getVertices();
        Edge[] edges = graph.getEdges();

        int arraysSize = Arrays.stream(vertices).max().getAsInt() + 1;
        Integer[] weights = new Integer[arraysSize]; //null = Inf
        Integer[] predecessors = new Integer[arraysSize];

        initWeights(srcVertex, weights);
        relaxWeights(vertices, edges, weights, predecessors);
        checkForNegativeWeightCycle(edges, weights);
        return new Pair<>(weights, predecessors);
    }

    private void initWeights(int srcVertex, Integer[] weights) {
        weights[srcVertex] = 0;
    }

    private void relaxWeights(int[] vertices, Edge[] edges, Integer[] weights, Integer[] predecessors) {
        for (int i = 0; i < vertices.length; i++) {
            for (Edge edge : edges) {
                int src = edge.getSrcVertex();
                int target = edge.getTargetVertex();
                int w = edge.getWeight();
                if (weights[src] == null) {
                    continue;
                }
                if ((weights[target] == null) || (weights[src] + w < weights[target])) {
                    weights[target] = weights[src] + w;
                    predecessors[target] = src;
                }
            }
        }
    }

    private void checkForNegativeWeightCycle(Edge[] edges, Integer[] weights) {
        for (Edge edge : edges) {
            if (weights[edge.getSrcVertex()] + edge.getWeight() < weights[edge.getTargetVertex()]) {
                throw new IllegalArgumentException("Graph contains a negative-weight cycle");
            }
        }
    }

}
