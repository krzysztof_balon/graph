package algo;

import java.lang.reflect.Array;
import java.util.NoSuchElementException;

public class Queue<T> {
    private static final int INITIAL_SIZE = 16;
    private final Class<T> elementType;
    private T[] queue;
    private int start = 0, end = -1;

    @SuppressWarnings("unchecked")
    public Queue(Class<T> elementType, int size) {
        this.elementType = elementType;
        this.queue = (T[]) Array.newInstance(elementType, size);
    }

    public Queue(Class<T> elementType) {
        this(elementType, INITIAL_SIZE);
    }

    public void add(T element) {
        extendAndZipQueue();
        queue[++end] = element;
    }

    public boolean haveMore() {
        return start <= end;
    }

    public T remove() {
        if (!haveMore()) {
            throw new NoSuchElementException();
        }
        return queue[start++];
    }

    public int size() {
        return 1 + end - start;
    }

    @SuppressWarnings("unchecked")
    private void extendAndZipQueue() {
        if (end + 1 >= queue.length) {
            T[] newQueue = (T[]) Array.newInstance(elementType, queue.length * 2);
            int size = size();
            System.arraycopy(queue, start, newQueue, 0, size);
            queue = newQueue;
            end = size + start - 1;
            start = 0;
        }
    }


}
