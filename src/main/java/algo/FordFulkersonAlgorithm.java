package algo;


import graph.Edge;
import graph.Graph;

import java.util.Arrays;
import java.util.Optional;

public class FordFulkersonAlgorithm {
    private final Graph graph;

    public FordFulkersonAlgorithm(Graph graph) {
        this.graph = graph;
    }

    public int getMaxFlow(int srcVertex, int targetVertex) {
        int maxFlow = 0;
        Edge[] path;
        while ((path = getPath(srcVertex, targetVertex)) != null) {
            int currentMaxFlow = Arrays.stream(path).mapToInt(Edge::getWeight).min().getAsInt();
            maxFlow += currentMaxFlow;
            for (Edge edge : path) {
                int resultWeight = edge.getWeight() - currentMaxFlow;
                if (resultWeight == 0) {
                    graph.removeEdge(edge.getSrcVertex(), edge.getTargetVertex());
                } else {
                    graph.editEdge(new Edge(edge.getSrcVertex(), edge.getTargetVertex(), resultWeight));
                }
                Optional<Edge> oppositeEdge = graph.getEdge(edge.getTargetVertex(), edge.getSrcVertex());
                if (!oppositeEdge.isPresent()) {
                    graph.addEdge(new Edge(edge.getTargetVertex(), edge.getSrcVertex(), resultWeight));
                } else {
                    int newWeight = oppositeEdge.get().getWeight() + resultWeight;
                    graph.editEdge(new Edge(edge.getTargetVertex(), edge.getSrcVertex(), newWeight));
                }
            }
        }
        return maxFlow;
    }

    private Edge[] getPath(int srcVertex, int targetVertex) {
        return new PathFinder(graph).getPath(srcVertex, targetVertex);
    }
}
