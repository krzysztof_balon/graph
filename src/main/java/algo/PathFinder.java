package algo;

import graph.Edge;
import graph.Graph;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class PathFinder {
    private final Graph graph;
    private final Queue<Integer> vertexQueue;
    private final Integer[] discoveredVertices;
    private int discoveredVerticesCounter;
    private final int[][] trace;
    private int traceCounter;

    public PathFinder(Graph graph) {
        this(graph, graph.getVertices().length);
    }

    private PathFinder(Graph graph, int verticesCount) {
        this(graph, new Queue<>(Integer.class, verticesCount),
                new Integer[verticesCount], new int[verticesCount][2]);
    }

    public PathFinder(Graph graph,
                      Queue<Integer> vertexQueue,
                      Integer[] discoveredVertices,
                      int[][] trace) {
        this.graph = graph;
        this.vertexQueue = vertexQueue;
        this.discoveredVertices = discoveredVertices;
        this.trace = trace;
    }

    public Edge[] getPath(int startVertex, int endVertex) {
        vertexQueue.add(startVertex);
        discover(startVertex);
        while (vertexQueue.haveMore()) {
            Integer vertex = vertexQueue.remove();
            if (vertex == endVertex) {
                int v = vertex;
                int i = discoveredVertices.length;
                Edge[] edges = new Edge[i];
                while (v != startVertex) {
                    int previousV = tracePrevious(v);
                    edges[--i] = graph.getEdge(previousV, v).get();
                    v = previousV;
                }
                int edgesNum = discoveredVertices.length - i;
                Edge[] zippedEdges = new Edge[edgesNum];
                System.arraycopy(edges, i, zippedEdges, 0, edgesNum);
                return zippedEdges;
            }
            for (Edge edgesFromVertex : graph.getEdgesFrom(vertex)) {
                int neighbor = edgesFromVertex.getTargetVertex();
                if (!alreadyDiscovered(neighbor)) {
                    vertexQueue.add(neighbor);
                    discover(neighbor);
                    trace(vertex, neighbor);
                }
            }
        }
        return null;
    }

    private int tracePrevious(int vertex) {
        for (int i = 0; i < traceCounter; i++) {
            if (trace[i][1] == vertex) {
                return trace[i][0];
            }
        }
        throw new NoSuchElementException();
    }

    private void discover(int vertex) {
        discoveredVertices[discoveredVerticesCounter++] = vertex;
    }

    private void trace(int previous, int current) {
        trace[traceCounter][0] = previous;
        trace[traceCounter++][1] = current;
    }

    private boolean alreadyDiscovered(int vertex) {
        return Arrays.stream(discoveredVertices)
                .filter(v -> v != null)
                .anyMatch(v -> v == vertex);
    }
}
